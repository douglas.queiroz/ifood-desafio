package com.br.desafio.core.presentation.component

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.core.module.Module

abstract class BaseComponentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        carregaModulos()
    }

    override fun onDestroy() {
        removeModulosCarregados()

        super.onDestroy()
    }

    private fun removeModulosCarregados() = unloadKoinModules(listModules())

    private fun carregaModulos() = loadKoinModules(listModules())

    abstract fun listModules(): List<Module>
}