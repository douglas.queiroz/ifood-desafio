package com.br.desafio.core.domain.navigator.contrato

import com.br.desafio.core.domain.navigator.IContrato

class HomeContrato(private val email: String) : IContrato {

    companion object {
        const val PARAM_NAME = "n"
    }

    override fun formataPath(): String {
        val pathHome = "home?$PARAM_NAME=%s"
        return String.format(pathHome, email)
    }
}