package com.br.desafio.core.domain.navigator.contrato

import com.br.desafio.core.domain.navigator.IContrato

class LoginContrato : IContrato {
    override fun formataPath(): String = "login"
}