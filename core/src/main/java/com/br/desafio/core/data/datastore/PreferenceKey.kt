package com.br.desafio.core.data.datastore

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferenceKey {
    val USUARIO_LOGADO = stringPreferencesKey("usuario_logado")
}