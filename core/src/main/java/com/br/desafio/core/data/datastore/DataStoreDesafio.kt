package com.br.desafio.core.data.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map

class DataStoreDesafio(private val dataStore: DataStore<Preferences>) {

    suspend fun <T> buscaValor(key: Preferences.Key<T>): T? {
        return dataStore.data.map {
            it[key]
        }.firstOrNull()
    }

    suspend fun <T> armazenaValor(key: Preferences.Key<T>, valor: T?) {
        dataStore.edit { settings ->
            valor?.let {
                settings[key] = it
            } ?: run {
                settings.remove(key)
            }
        }
    }

    suspend fun <T> removeValor(key: Preferences.Key<T>) = armazenaValor(key, null)
}