package com.br.desafio.core.domain.model

import androidx.annotation.StringRes

sealed class BaseResult<out T : Any> {

    data class Success<out T : Any>(val data: T) : BaseResult<T>()
    data class Error(@StringRes val error: Int) : BaseResult<Nothing>()
    data class Loading(val showLoading: Boolean = true) : BaseResult<Nothing>()
}