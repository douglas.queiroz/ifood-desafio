package com.br.desafio.home.domain.model

import com.br.desafio.home.domain.model.entity.Card
import com.google.gson.annotations.SerializedName

data class RespostaCard(@SerializedName("Classic") val cards: List<Card>)
