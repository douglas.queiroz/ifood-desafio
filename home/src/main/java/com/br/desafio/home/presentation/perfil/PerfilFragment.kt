package com.br.desafio.home.presentation.perfil

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.br.desafio.core.domain.navigator.DesafioNavigator
import com.br.desafio.core.domain.navigator.contrato.LoginContrato
import com.br.desafio.home.databinding.HomeFragmentPerfilBinding
import com.br.desafio.home.presentation.base.BaseHomeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class PerfilFragment : BaseHomeFragment() {

    private val perfilViewModel: PerfilViewModel by viewModel()
    private var _binding: HomeFragmentPerfilBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = HomeFragmentPerfilBinding.inflate(inflater, container, false)
        val root: View = binding.root

        getHomeActivity().supportActionBar?.title = getHomeActivity().nomeUsuario

        initObserver()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSair.setOnClickListener {
            perfilViewModel.sair()
        }
    }

    private fun initObserver() {
        perfilViewModel.sair.observe(viewLifecycleOwner) {
            DesafioNavigator.navegacaoSemVolta(requireActivity(), LoginContrato())
            getHomeActivity().finish()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}