package com.br.desafio.home.data.repository

import com.br.desafio.core.data.datastore.DataStoreDesafio
import com.br.desafio.core.data.datastore.PreferenceKey
import com.br.desafio.home.domain.irepository.IPerfilRepository


class PerfilRepository(private val dataStoreDesafio: DataStoreDesafio) : IPerfilRepository {

    override suspend fun sair() {
        dataStoreDesafio.removeValor(PreferenceKey.USUARIO_LOGADO)
    }
}