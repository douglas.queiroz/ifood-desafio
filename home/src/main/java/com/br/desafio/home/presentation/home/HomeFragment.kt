package com.br.desafio.home.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.home.databinding.HomeFragmentBinding
import com.br.desafio.home.domain.model.entity.Card
import com.br.desafio.home.presentation.base.BaseHomeFragment
import com.br.desafio.home.presentation.home.adapter.CardListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseHomeFragment() {

    private val homeViewModel: HomeViewModel by viewModel()
    private var _binding: HomeFragmentBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initObserver()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.buscaCards()
    }

    private fun initObserver() {
        homeViewModel.listCard.observe(viewLifecycleOwner) {
            if (it is BaseResult.Success) {
                sucessoLista(it.data)
            } else if (it is BaseResult.Error) {
                falhaLista(it.error)
            }

            binding.homeProgressbar.visibility = View.GONE
        }
    }

    private fun sucessoLista(data: List<Card>) {
        binding.textHome.visibility = View.GONE
        binding.cardLista.adapter = CardListAdapter(data) {
            navigate(
                HomeFragmentDirections.homeActionNavigationHomeToNavigationDetail(it)
            )
        }

        binding.cardLista.layoutManager = LinearLayoutManager(requireContext())
        binding.cardLista.visibility = View.VISIBLE
    }

    private fun falhaLista(errorId: Int) {
        binding.textHome.text = getString(errorId)
        binding.textHome.visibility = View.VISIBLE
        binding.cardLista.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}