package com.br.desafio.home.data.repository

import android.util.Log
import com.br.desafio.home.data.database.dao.CardDAO
import com.br.desafio.home.domain.model.entity.Card
import com.br.desafio.home.data.request.CardApi
import com.br.desafio.home.domain.irepository.IHomeRepository

class HomeRepository(private val cardApi: CardApi, private val cardDAO: CardDAO) : IHomeRepository {

    override suspend fun buscaCards(): List<Card> {
        var listCard = cardDAO.getAll()

        if (listCard.isNullOrEmpty()) {
            listCard = buscaCardApi()
        }

        Log.d(this::class.java.name, listCard.toString())

        return listCard
    }

    private suspend fun buscaCardApi(): List<Card> {
        val resultAPi = cardApi.getUsers().body()?.cards
        resultAPi?.let {
            cardDAO.insert(it)
            return@buscaCardApi it
        }

        return emptyList()
    }
}