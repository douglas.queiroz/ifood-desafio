package com.br.desafio.home.presentation.base

import com.br.desafio.core.presentation.component.BaseComponentFragment
import com.br.desafio.home.presentation.HomeActivity

abstract class BaseHomeFragment : BaseComponentFragment() {

    fun getHomeActivity() = requireActivity() as HomeActivity
}