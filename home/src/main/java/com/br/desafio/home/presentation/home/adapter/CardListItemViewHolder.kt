package com.br.desafio.home.presentation.home.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.br.desafio.home.R
import com.br.desafio.home.domain.model.entity.Card
import com.bumptech.glide.Glide

class CardListItemViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {

    fun bind(card: Card) {
        itemView.findViewById<TextView>(R.id.cardName).text = card.name
        itemView.findViewById<TextView>(R.id.cardTipo).text = card.type

        Glide
            .with(itemView)
            .load(card.img)
            .circleCrop()
            .placeholder(R.drawable.home_ic_loading)
            .into(itemView.findViewById(R.id.picture))
    }
}