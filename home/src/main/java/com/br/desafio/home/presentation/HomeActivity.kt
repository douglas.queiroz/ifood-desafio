package com.br.desafio.home.presentation

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.br.desafio.core.domain.navigator.contrato.HomeContrato
import com.br.desafio.core.presentation.component.BaseComponentActivity
import com.br.desafio.home.R
import com.br.desafio.home.databinding.HomeActivityBinding
import com.br.desafio.home.domain.module.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.core.module.Module

class HomeActivity : BaseComponentActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: HomeActivityBinding
    lateinit var nomeUsuario: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = HomeActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        nomeUsuario = intent.data?.getQueryParameter(HomeContrato.PARAM_NAME) ?: ""
        configuraNavController()
    }


    private fun configuraNavController() {
        val navView: BottomNavigationView = binding.navView

        navController = findNavController(R.id.nav_host_fragment_activity_home)
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.navigation_home, R.id.navigation_perfil)
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        binding.navView.setOnNavigationItemReselectedListener {
            return@setOnNavigationItemReselectedListener
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    override fun listModules(): List<Module> =
        listOf(
            singleModuleHome,
            roomModuleHome,
            casesModuleHome,
            repositoryModuleHome,
            viewModelModuleHome
        )
}