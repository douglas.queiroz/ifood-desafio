package com.br.desafio.home.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.br.desafio.core.data.database.BaseDAO
import com.br.desafio.home.domain.model.entity.Card

@Dao
interface CardDAO : BaseDAO<Card> {

    @Query("SELECT c.* from card c where c.cardId=:key")
    suspend fun get(key: Int): Card

    @Query("SELECT c.* from card c")
    suspend fun getAll(): List<Card>?

    @Query("DELETE FROM Card")
    suspend fun clear()
}