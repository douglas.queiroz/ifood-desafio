package com.br.desafio.home.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.br.desafio.home.databinding.HomeDetailBinding
import com.br.desafio.home.presentation.base.BaseHomeFragment
import com.bumptech.glide.Glide

class DetailFragment : BaseHomeFragment() {

    private val detailFragmentArgs: DetailFragmentArgs by lazy {
        DetailFragmentArgs.fromBundle(requireArguments())
    }

    private var _binding: HomeDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = HomeDetailBinding.inflate(inflater, container, false)

        getHomeActivity().supportActionBar?.title = detailFragmentArgs.card.name

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(detailFragmentArgs.card.img)
            .into(binding.picute)
    }
}