package com.br.desafio.home.data.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.br.desafio.home.data.database.dao.CardDAO
import com.br.desafio.home.domain.model.entity.Card

@Database(entities = [Card::class], version = 1, exportSchema = true)
abstract class DesafioDatabase : RoomDatabase() {

    abstract val cardDAO: CardDAO

    companion object {
        @Volatile
        private var INSTANCE: DesafioDatabase? = null

        fun getIntanceDatabase(context: Application): DesafioDatabase? {
            synchronized(this) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        DesafioDatabase::class.java,
                        "desafio-db"
                    )
                        .build()
                }
            }

            return INSTANCE
        }

        fun destroyerInstance() {
            INSTANCE = null
        }
    }
}