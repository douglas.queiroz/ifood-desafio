package com.br.desafio.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.domain.model.LoggedInUser
import com.br.desafio.domain.model.LoginFormState
import com.br.desafio.domain.usercase.LoginUserCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(private val loginUserCase: LoginUserCase) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<BaseResult<LoggedInUser>>()
    val loginResult: LiveData<BaseResult<LoggedInUser>> = _loginResult


    fun verificaUsuarioLogado() {
        viewModelScope.launch(Dispatchers.Default) {
            when (val result = loginUserCase.verificaUsuarioLogado()) {
                is BaseResult.Error -> {
                    _loginForm.postValue(LoginFormState(isLoginManual = true))
                }
                else -> {
                    _loginResult.postValue(result)
                }
            }
        }
    }

    fun login(username: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _loginResult.postValue(loginUserCase.login(username, password))
        }
    }

    fun loginDataChanged(username: String, password: String) {
        _loginForm.value = loginUserCase.loginDataChanged(username, password)
    }
}