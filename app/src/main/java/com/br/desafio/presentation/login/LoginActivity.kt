package com.br.desafio.presentation.login

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.br.desafio.R
import com.br.desafio.core.domain.model.BaseResult
import com.br.desafio.core.domain.navigator.DesafioNavigator
import com.br.desafio.core.domain.navigator.contrato.HomeContrato
import com.br.desafio.databinding.ActivityLoginBinding
import com.br.desafio.domain.model.LoggedInUser
import com.br.desafio.presentation.extesion.afterTextChanged
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initObserver()

        loginViewModel.verificaUsuarioLogado()

        listenerInputs()
    }

    private fun listenerInputs() {
        binding.username.afterTextChanged { dataChanged() }

        binding.password.apply {
            afterTextChanged { dataChanged() }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> login()
                }
                false
            }

            binding.login.setOnClickListener {
                binding.loading.visibility = View.VISIBLE
                login()
            }
        }
    }

    private fun login() {
        loginViewModel.login(
            binding.username.text.toString(),
            binding.password.text.toString()
        )
    }

    private fun dataChanged() {
        loginViewModel.loginDataChanged(
            binding.username.text.toString(),
            binding.password.text.toString()
        )
    }

    private fun initObserver() {
        initFormObserver()
        initLoginObserver()
    }

    private fun initLoginObserver() {
        loginViewModel.loginResult.observe(this@LoginActivity) {
            it.let { result ->
                binding.loading.visibility = View.GONE

                when (result) {
                    is BaseResult.Success -> {
                        updateUiWithUser(result.data)
                        DesafioNavigator.navegacaoSemVolta(this, HomeContrato(result.data.userId))
                    }
                    else -> {
                    }
                }
            }
        }
    }

    private fun initFormObserver() {
        loginViewModel.loginFormState.observe(this@LoginActivity) {
            it.let { form ->
                binding.login.isEnabled = form.isDataValid

                form.usernameError?.let { error ->
                    binding.username.error = getString(error)
                }
                form.passwordError?.let { error ->
                    binding.password.error = getString(error)
                }

                if (form.isLoginManual) {
                    loginManual()
                }
            }
        }
    }

    private fun loginManual() {
        binding.username.visibility = View.VISIBLE
        binding.password.visibility = View.VISIBLE
        binding.login.visibility = View.VISIBLE
        binding.loading.visibility = View.GONE
    }

    private fun updateUiWithUser(model: LoggedInUser) =
        Toast.makeText(
            applicationContext,
            "${getString(R.string.welcome)} ${model.displayName}",
            Toast.LENGTH_LONG
        ).show()

    private fun showLoginFailed(@StringRes errorString: Int) =
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
}