package com.br.desafio.data.source

import com.br.desafio.domain.model.LoggedInUser

class LoginDataSource {

    fun loginFake(username: String, password: String): LoggedInUser? {
        return try {
            LoggedInUser(username, username)
        } catch (e: Throwable) {
            null
        }
    }
}