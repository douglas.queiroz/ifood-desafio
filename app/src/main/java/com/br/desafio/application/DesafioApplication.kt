package com.br.desafio.application

import com.br.desafio.application.module.*
import com.br.desafio.core.application.BaseApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level

class DesafioApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()

        initKoin()
    }

    private fun initKoin() {
        stopKoin()

        startKoin {
            printLogger(Level.DEBUG)
            androidContext(this@DesafioApplication)
            modules(
                listOf(
                    singleModule,
                    serviceModule,
                    repositoryModule,
                    userCaseModule,
                    viewModelModule
                )
            )
        }
    }

}

